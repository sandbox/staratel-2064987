<?php

/**
 * @file
 * Bulk export of views_default objects for Wsclient Recurly Views.
 */

/**
 * Implements hook_views_default_views()
 */
function wsclient_recurly_views_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'recurly_accounts';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'recurly:accounts';
  $view->human_name = 'Recurly: List Accounts';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Recurly: List Accounts';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'account_code' => 'account_code',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'account_code' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: WS: Recurly: Account code */
  $handler->display->display_options['fields']['account_code']['id'] = 'account_code';
  $handler->display->display_options['fields']['account_code']['table'] = 'recurly:accounts';
  $handler->display->display_options['fields']['account_code']['field'] = 'account_code';
  /* Contextual filter: WS: Recurly: state */
  $handler->display->display_options['arguments']['state']['id'] = 'state';
  $handler->display->display_options['arguments']['state']['table'] = 'recurly:accounts';
  $handler->display->display_options['arguments']['state']['field'] = 'state';
  $handler->display->display_options['arguments']['state']['default_action'] = 'default';
  $handler->display->display_options['arguments']['state']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['state']['default_argument_options']['argument'] = 'active';
  $handler->display->display_options['arguments']['state']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['state']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['state']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['state']['limit'] = '0';
  /* Contextual filter: WS: Recurly: per_page */
  $handler->display->display_options['arguments']['per_page']['id'] = 'per_page';
  $handler->display->display_options['arguments']['per_page']['table'] = 'recurly:accounts';
  $handler->display->display_options['arguments']['per_page']['field'] = 'per_page';
  $handler->display->display_options['arguments']['per_page']['default_action'] = 'default';
  $handler->display->display_options['arguments']['per_page']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['per_page']['default_argument_options']['argument'] = '50';
  $handler->display->display_options['arguments']['per_page']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['per_page']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['per_page']['summary_options']['items_per_page'] = '25';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'recurly-accounts';
  $translatables['recurly_accounts'] = array(
    t('Master'),
    t('Recurly: List Accounts'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Account code'),
    t('All'),
    t('Page'),
  );

  $views['recurly_accounts'] = $view;

  return $views;
}
