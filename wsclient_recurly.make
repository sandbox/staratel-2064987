; Core version
; ------------
; Each makefile should begin by declaring the core version of Drupal that all
; projects should be compatible with.
core = 7.x
; API version
; ------------
; Every makefile needs to declare it's Drush Make API version. This version of
; drush make uses API version `2`.
api = 2

; Dependencies.
projects[entity][overwrite] = TRUE
projects[entity][version] = 1.x-dev
projects[http_client][overwrite] = TRUE
projects[http_client][version] = 2.x-dev
projects[views][overwrite] = TRUE
projects[views][version] = 3.x-dev
projects[wsclient][overwrite] = TRUE
projects[wsclient][version] = 1.x-dev
; As wsclient_views is not a regular a module but a sandbox, specify source directly.
projects[wsclient_views][type] = module
projects[wsclient_views][download][type] = git
projects[wsclient_views][download][url] = http://git.drupal.org/sandbox/wesnick/1437134.git
projects[wsclient_views][download][branch] = 7.x-1.x
projects[wsclient_views][overwrite] = TRUE
projects[wsclient_views][version] = 1.x-dev
; For wsclient_ui.
projects[rules][overwrite] = TRUE
projects[rules][version] = 2.x-dev

; Patches.
; The following twosome add cool auth configuration. Commented as they cause merge conflicts.
;projects[http_client][patch][] = "http://drupal.org/files/http_client-2042205-create_basic_http_auth_plugin-2.patch"
;projects[wsclient][patch][] = "http://drupal.org/files/wsclient-1285310-http_basic_authentication-14.patch"
projects[wsclient][patch][] = "http://drupal.org/files/wsclient-request_alter-1934274-7.patch"
projects[wsclient_views][patch][] = "http://drupal.org/files/wsclient_views-2044669-recover_workability-3.patch"
